﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ShoppingList.Models;

namespace ShoppingList.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["MachineName"] = Environment.MachineName;
            ViewData["OsDescription"] = RuntimeInformation.OSDescription;
            ViewData["Architecture"] = RuntimeInformation.OSArchitecture;
            ViewData["NetFramework"] = RuntimeInformation.FrameworkDescription;

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
