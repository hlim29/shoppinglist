﻿using Microsoft.EntityFrameworkCore;

namespace ShoppingList.Models
{
    public class ShoppingListContext : DbContext
    {
        public ShoppingListContext(DbContextOptions<ShoppingListContext> options)
            : base(options)
        {
        }

        public DbSet<Item> Items { get; set; }
    }

    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public decimal Cost { get; set; }
        public PurchaseStatus Status {get;set;}
    }

    public enum PurchaseStatus
    {
        NotBought, PartiallyBought, Bought
    }
}
