﻿IF OBJECT_ID(N'__EFMigrationsHistory') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [Items] (
    [Id] int NOT NULL IDENTITY,
    [Amount] decimal(18, 2) NOT NULL,
    [Name] nvarchar(max) NULL,
    [Status] int NOT NULL,
    CONSTRAINT [PK_Items] PRIMARY KEY ([Id])
);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20181125050101_InitialCreate', N'2.0.3-rtm-10026');

GO

EXEC sp_rename N'Items.Amount', N'Cost', N'COLUMN';

GO

ALTER TABLE [Items] ADD [Quantity] int NOT NULL DEFAULT 0;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20181125051342_UpdateCostAndAmount', N'2.0.3-rtm-10026');

GO

